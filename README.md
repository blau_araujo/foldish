# Fold in Shell (foldish)

O utilitário `fold` (GNU coreutils), que quebra linhas de texto após um determinado número de colunas, tem o inconveniente de calcular a quantidade de colunas, não com base no número de caracteres, mas no número de bytes. Isso faz com que ele seja praticamente inútil quando a linha contiver caracteres multibyte (como caracteres acentuados e a cedilha, por exemplo).

Considere a string `line` e a régua `rule`:

```
:~$ line='Preste atenção onde as quebras ocorrerão. Senão, você não entenderá a situação.'
:~$ rule='1234|6789|1234|6789|1234|6789|'
```

Veja o que acontece com o `fold`:

```
:~$ echo $rule; fold -sw30 <<< "$line"
1234|6789|1234|6789|1234|6789|
Preste atenção onde as
quebras ocorrerão. Senão,
você não entenderá a
situação.
```

O objetivo do `foldish` é cobrir essa deficiência.

```
:~$ echo $rule; ./foldish 30 <<< "$line"
1234|6789|1234|6789|1234|6789|
Preste atenção onde as quebras
ocorrerão. Senão, você não
entenderá a situação.
```

## Uso

Modo interativo:

```
foldish [COLUNA] ARQUIVO
foldish [OPÇÕES]

OPÇÕES:

    -h|--help     Exibe ajuda e sai.
    -v|--version  Exibe versão e sai.
```

Modo não-interativo:

```
foldish [COLUNA] < ARQUIVO
COMANDO | foldish [COLUNA]
```

Em ambos os casos, a omissão de `COLUNA` fará com que a linha seja quebrada na coluna 80.

> O `foldish` sempre quebra linhas nos espaços, a menos que não existam espaços na linha.

## Instalação

Esta é uma versão de desenvolvimento e não temos um instalador. Para testar, será preciso clonar o repositório e copiar (ou *linkar*) o script `foldish` para o local da sua preferência.


